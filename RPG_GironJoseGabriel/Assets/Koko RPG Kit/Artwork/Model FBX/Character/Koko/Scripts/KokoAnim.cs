﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoAnim : MonoBehaviour {

    public Animator animator;

	// Use this for initialization
	void Start ()
    {
        animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.A))
        {
            animator.SetTrigger("Idle");

        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            animator.SetTrigger("Run");

        }
    }
}
