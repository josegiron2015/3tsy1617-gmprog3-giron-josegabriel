﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

//TODO: Create a StateMachine
//Run
//Idle
//ATK
//Death
//Skill
//...

public class PlayerMovement : MonoBehaviour 
{
    public GameObject Pointer;
	NavMeshAgent navMeshAgent;
	Animator anim;
	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator> ();
		navMeshAgent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // another conversion chu
		RaycastHit hit;

		if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000.0f))
		{
            anim.SetTrigger("RunNo");
            Pointer.SetActive(true);
            Pointer.transform.position = hit.point;			
			this.navMeshAgent.SetDestination (hit.point);

            


        }
        if (Vector3.Distance(this.transform.position, Pointer.transform.position) <= 0.1f)
        {
            Pointer.SetActive(false);
            anim.SetTrigger("Idle");
        }

    }


}
