﻿using UnityEngine;
using UnityEngine.EventSystems;

public class KokoIdleState :  IKokoState
{
    private readonly KokoStatePattern Player;

    public KokoIdleState(KokoStatePattern statePattern)
    {
        Player = statePattern;
    }

    public void UpdateState()
    {
        RemoveOtherAnimations();
        RaycastToTarget();
    }

    public void OnTriggerEnter(Collider other)
    {
    }

    public void ToKokoMoveState()
    {
        Player.kokocurrentState = Player.kokomoveState;
    }

    public void ToKokoIdleState()
    {
        Debug.Log("Can't transition to same state");
    }

    public void ToKokoChaseState()
    {
        Player.kokocurrentState = Player.kokochaseState;

    }

    public void ToKokoAttackState()
    {
        Player.kokocurrentState = Player.kokoattackState;

    }

    public void RaycastToTarget()
    {

        // Destroy(Pointer);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // another conversion chu
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray,out hit,1000.0f))
        {
            if (EventSystem.current.IsPointerOverGameObject())

            {
               
                //Do not do anything if click on canvas
            }

            else {

                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.MonsterLayer)) // if monster was hit
                    {
                        Debug.Log("MONSTER HIT");
                        Player.Target = hit.collider.gameObject;// Hit Terrain make terrain as target position                      
                        Player.anim.SetTrigger("Run");
                        ToKokoChaseState();
                    }

                    else if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.GroundLayer)) // if terrain was hit
                    {
                        Debug.Log("TERRAIN HIT");
                        Player.TargetPosition = hit.point; // Hit Terrain make terrain as target position
                        Player.anim.SetTrigger("RunNo");
                        ToKokoMoveState();
                    }
                }
            }
        
    }

    void RemoveOtherAnimations()
    {
       
        Player.anim.ResetTrigger("Attack");
        //Player.anim.ResetTrigger("RunNo"); // Fix the bug in which when you click near the player, it will play the running animation even if IdleState

    }
}


