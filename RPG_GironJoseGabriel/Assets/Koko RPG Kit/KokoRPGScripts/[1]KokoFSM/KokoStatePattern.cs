﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class KokoStatePattern : MonoBehaviour 
{
    public float WalkRange;
	public float IdleTime;
	public float ChaseRange;
	public float AttackRange;
    public float StopRange;
    public float speed;
    public Vector3 TargetPosition;
    public GameObject Target;
    public GameObject Pointer;
	public Animator anim;
	public MeshRenderer meshRendererFlag;
    public LayerMask GroundLayer;
    public LayerMask MonsterLayer;


   public CharacterController charactercontroller;

	[HideInInspector] public IKokoState kokocurrentState;
	[HideInInspector] public KokoChaseState kokochaseState;
	[HideInInspector] public KokoMoveState kokomoveState;
	[HideInInspector] public KokoIdleState kokoidleState;
	[HideInInspector] public KokoAttackState kokoattackState;
	[HideInInspector] public UnityEngine.AI.NavMeshAgent navMeshAgent;


	private void Awake()
	{
		kokochaseState = new KokoChaseState (this);
		kokomoveState = new KokoMoveState (this);
		kokoidleState = new KokoIdleState (this);
		kokoattackState = new KokoAttackState (this);
		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
	}

	void Start () 
	{

		anim = GetComponent<Animator>();
        charactercontroller = gameObject.GetComponent<CharacterController>();
        kokocurrentState = kokoidleState;
     
	}

	void Update () 
	{
        print(kokocurrentState.GetType().ToString());
        kokocurrentState.UpdateState(); // do not delete... Makes states be states
    }

    private void OnTriggerEnter(Collider other)
    {

    }
}