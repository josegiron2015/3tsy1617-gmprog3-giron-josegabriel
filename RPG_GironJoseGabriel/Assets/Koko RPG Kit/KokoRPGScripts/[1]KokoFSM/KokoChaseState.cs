﻿using UnityEngine;
using UnityEngine.EventSystems;

public class KokoChaseState :  IKokoState 

{	
	public Animator anim;
	private readonly KokoStatePattern Player;
	//private float Timer = 0.0f;

	public KokoChaseState (KokoStatePattern statePattern)
	{
		Player = statePattern;
	}

	public void UpdateState()
	{
        RemoveAttackAnimation();
        RaycastToTarget();	
	    Chase ();
	}

	public void OnTriggerEnter (Collider other)
	{
        
    }

	public void ToKokoMoveState()
	{
        Player.kokocurrentState = Player.kokomoveState;
	}

	public void ToKokoChaseState()
	{
        Player.kokocurrentState = Player.kokochaseState;
	}

	public void ToKokoIdleState()
	{
		Player.kokocurrentState = Player.kokoidleState;
	}

	public void ToKokoAttackState()
	{
		Player.kokocurrentState = Player.kokoattackState;
	}

    public void RaycastToTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // another conversion chu
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000.0f))
        { 
            if (EventSystem.current.IsPointerOverGameObject())

            {
                //Do not do anything if click on canvas
            }
            else {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.MonsterLayer)) // if monster was hit
                {

                    Player.Target = hit.collider.gameObject; // Hit Terrain make terrain as target position
                    
                    
                    Player.anim.SetTrigger("Run");
                }

                else if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.GroundLayer))// if terrain was hit
                {                  
                    //Debug.Log("TERRAIN HIT MOVE STATE");
                    Player.TargetPosition = hit.point; // Hit Terrain make terrain as target position
                    Player.anim.SetTrigger("RunNo");
                    ToKokoMoveState();
                    
                }
            }
        }
    }

    private void Chase()
    {
        Vector3 PlayerChaseMovementPosition = Player.Target.transform.position - Player.transform.position;
        PlayerChaseMovementPosition.Normalize();

        Player.transform.LookAt(Player.Target.transform.position);
        if (Player.Target != null)
        {
			          
            if (Vector3.Distance(Player.Target.transform.position, Player.transform.position) > Player.ChaseRange)
            {
                Player.charactercontroller.Move(PlayerChaseMovementPosition * Player.speed * Time.deltaTime);

            }
            else if (Vector3.Distance( Player.Target.transform.position, Player.transform.position) <= Player.ChaseRange)
            {

                ToKokoAttackState();
            }
        }

        else
        {
            
            ToKokoMoveState();
        }
  
    }

    void Rotate()
    {
        if (Player.Target.transform.position != Player.transform.position)
        {
            Quaternion rot = Quaternion.LookRotation(Player.TargetPosition - Player.transform.position);
            float rotationX = rot.eulerAngles.y;
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            Player.transform.localRotation = Quaternion.Slerp(Player.transform.localRotation, xQuaternion, Time.deltaTime * 120f);
        }
    }
    void RemoveAttackAnimation()
    {
        Player.anim.ResetTrigger("Attack");
    }
}