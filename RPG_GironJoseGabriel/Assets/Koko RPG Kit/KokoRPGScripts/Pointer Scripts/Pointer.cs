﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Pointer : MonoBehaviour {


	public GameObject DefaultPointer; // object to Instantiate
	public GameObject EnemyPointer; // object to Instantiate

	public GameObject DefPointer;    
	public GameObject EnPointer;

	public LayerMask GroundLayer;
	public LayerMask MonsterLayer;

	// Use this for initialization
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			Debug.Log("Player on hit");
			Destroy(this.gameObject);
		}
	}

	// Update is called once per frame
	void Update ()
	{
		RaycastToTarget();	
	}

	public void RaycastToTarget ()
	{

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //
		RaycastHit hit;

		if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000.0f))
		{

			Destroy(DefPointer);
			Destroy(EnPointer);

			if (EventSystem.current.IsPointerOverGameObject())

			{
				//Do not do anything if click on canvas
			}

			else {

				if (Physics.Raycast(ray, out hit, 1000.0f, MonsterLayer))
				{
					EnPointer = Instantiate(EnemyPointer, hit.collider.gameObject.transform.position, Quaternion.identity) as GameObject;
					EnPointer.transform.SetParent(hit.collider.gameObject.transform);

				}

				else if (Physics.Raycast(ray, out hit, 1000.0f, GroundLayer))
				{
					DefPointer = Instantiate(DefaultPointer, hit.point, Quaternion.identity) as GameObject;
				}								
			}
		}
	}		

}