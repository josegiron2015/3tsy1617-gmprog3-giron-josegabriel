﻿using UnityEngine;
using System.Collections;

public class NPCAnimation : MonoBehaviour
{

    private Animator animator;


    void Start()
    {
        //Components
        animator = GetComponent<Animator>();

    }

    public void AnimationController(string animation)
    {
        animator.SetTrigger(animation);
    }
}