﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour
{

    private Animator animator;

    public GameObject KokoCamera;
    public GameObject MonstersCamera;
    public GameObject NPCCamera;

    public GameObject KokoPanel;
    public GameObject MonstersPanel;
    public GameObject NPCPanel;
    void Start()
    {
        //Components
        animator = GetComponent<Animator>();

    }
    public void AnimationController(string animation)
    {
        animator.SetTrigger(animation);
    }

    public void KokoCam()
    {
        Camera.main.transform.position = KokoCamera.transform.position;
        KokoPanel.SetActive(true);
        MonstersPanel.SetActive(false);
        NPCPanel.SetActive(false);
    }

    public void MonstersCam()
    {
        Camera.main.transform.position = MonstersCamera.transform.position;
        MonstersPanel.SetActive(true);
        KokoPanel.SetActive(false);
        NPCPanel.SetActive(false);
    }

    public void NPCCCam()
    {
        Camera.main.transform.position = NPCCamera.transform.position;
        NPCPanel.SetActive(true);
        KokoPanel.SetActive(false);
        MonstersPanel.SetActive(false);
    }
}