﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour {

    float offsetY = 2;

	void Start ()
    {
        LeanTween.moveLocalY(this.gameObject, this.transform.position.y + offsetY, 1).setEase(LeanTweenType.easeInCubic);
        LeanTween.scale(this.gameObject, new Vector3(.15f,.15f,.15f), .2f).setLoopPingPong(1);
        LeanTween.alpha(this.gameObject, 0, .5f).setDelay(.5f);
    }
	
}
