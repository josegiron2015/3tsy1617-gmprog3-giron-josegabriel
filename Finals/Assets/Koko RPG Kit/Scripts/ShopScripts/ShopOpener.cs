﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopOpener : MonoBehaviour {

	public GameObject Shop;
	public GameObject DialogueBox;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
		{
			DialogueBox.SetActive(!DialogueBox.activeSelf);
			Debug.Log ("Player Shop Detected");

			if (Input.GetKeyDown(KeyCode.E)) 
			{
				Shop.SetActive(!Shop.activeSelf);
				Debug.Log ("Shop Enabled");
			}
		}
	}
		
}
