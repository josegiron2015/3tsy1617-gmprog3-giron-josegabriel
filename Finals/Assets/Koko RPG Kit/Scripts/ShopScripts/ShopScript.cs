﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopScript : MonoBehaviour {

	public GameObject Shop;
	public GameObject Koko;
	//public GameObject InventoryReference;
	public Inventory inventory;
	public GameObject ItemForSale; //What Item
	public GameObject ItemForSale2; //What Item
	public GameObject ItemForSale3; //What Item

	public float Item1Price;
	public float Item2Price;
	public float Item3Price;
	// Use this for initialization
	void Start () 
	{
		//Koko = gameObject.GetComponent<Player> ();
		//InventoryReference = gameObject.GetComponent<Inventory> ();
		//InventoryReference.GetComponent<Inventory> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Player"))
			{
				if (Input.GetKeyDown (KeyCode.E)) 
				{
				Shop.SetActive(!Shop.activeSelf);
				}
			}
	}

	public void BuyPotion()
	{
		if(Koko.gameObject.GetComponent<Player> ().GoldCount < Item1Price)
		{
			Debug.Log ("NOT ENOUGH GOLD FOR HP POTION");
			return;
		}
		Debug.Log ("Bought Potion");
		Koko.gameObject.GetComponent<Player> ().GoldCount -= Item1Price;
		Debug.Log (Koko.gameObject.GetComponent<Player> ().GoldCount.ToString ());
		inventory.AddItem(ItemForSale.GetComponent<Item>()); //Adds the item to the inventory.
		//InventoryReference = 
	
	}

	public void BuyManaPotion()
	{
		if(Koko.gameObject.GetComponent<Player> ().GoldCount < Item2Price)
		{
			Debug.Log ("NOT ENOUGH GOLD FOR MANA POTION");
			return;
		}
		Debug.Log ("Bought Potion");
		Koko.gameObject.GetComponent<Player> ().GoldCount -= Item2Price;
		Debug.Log (Koko.gameObject.GetComponent<Player> ().GoldCount.ToString ());
		inventory.AddItem(ItemForSale2.GetComponent<Item>()); //Adds the item to the inventory.
		//InventoryReference = 

	}

	public void BuyBuffPotion()
	{

		if(Koko.gameObject.GetComponent<Player> ().GoldCount < Item3Price)
		{
			Debug.Log ("NOT ENOUGH GOLD FOR BUFF POTION");
			return;
		}
		Debug.Log ("Bought Potion");
		Koko.gameObject.GetComponent<Player> ().GoldCount -= Item3Price;
		Debug.Log (Koko.gameObject.GetComponent<Player> ().GoldCount.ToString ());
		inventory.AddItem(ItemForSale3.GetComponent<Item>()); //Adds the item to the inventory.
		//InventoryReference = 

	}
}



//public void ActivateAndDeactivate()
//{
//	image.SetActive(!image.activeSelf);
//}