﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnScript : MonoBehaviour
{


    // Use this for initialization
    public List<GameObject> spawnList;
    public int CurrentSpawnCount;
    public int MaximumSpawnCount;
    //public PlayerHealth playerHealth;       // Reference to the player's heatlh.
    // public GameObject enemy;                // The enemy prefab to be spawned.
    private float TimeElapsed;
    public float spawnTime = 3f;            // How long between each spawn.
    public float timer = 0.0f;

    public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
    Vector3 SpawnArea;

    bool spawn = true;
    //public int AreaSpawnEffect;
    void Start()
    {
		
        //spawnList = new List<GameObject>();
        //InvokeRepeating("Spawn", spawnTime, spawnTime);

    }
    void Update()
    {
        if (spawn)
        {
            timer += Time.deltaTime;

            if (timer > spawnTime)
            {
                if (CurrentSpawnCount < MaximumSpawnCount)
                {
                    Spawn();
                    timer = 0.0f;
                }
                else
                {
                    spawn = false;
                    Debug.Log("SPAWN LIMIT MAX");
                }
            }
        }
    }


    void Spawn()
    {

        SpawnArea = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
		Instantiate(spawnList[Random.Range(0, spawnList.Count)], spawnPoints[spawnPointIndex].position + SpawnArea, spawnPoints[spawnPointIndex].rotation);
        CurrentSpawnCount++;


    }

}


