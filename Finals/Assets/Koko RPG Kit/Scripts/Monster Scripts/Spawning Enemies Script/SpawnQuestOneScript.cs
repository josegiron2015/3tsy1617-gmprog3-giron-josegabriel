﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnQuestOneScript : MonoBehaviour
{


	// Use this for initialization
	public GameObject spawnList;
	public GameObject spawnList2;
	public GameObject spawnList3;

	public int CurrentSpawnCount;
	public int MaximumSpawnCount;

	public int CurrentSpawnCount1;
	public int MaximumSpawnCount1;

	public int CurrentSpawnCount2;
	public int MaximumSpawnCount2;
	//public PlayerHealth playerHealth;       // Reference to the player's heatlh.
	// public GameObject enemy;                // The enemy prefab to be spawned.
	private float TimeElapsed;
	public float spawnTime = 3f;            // How long between each spawn.
	public float timer = 0.0f;

	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
	Vector3 SpawnArea;

	bool spawn = true;
	//public int AreaSpawnEffect;
	void Start()
	{

		//spawnList = new List<GameObject>();
		//InvokeRepeating("Spawn", spawnTime, spawnTime);

	}
	void Update()
	{
		if (spawn)
		{
			timer += Time.deltaTime;

			if (timer > spawnTime)
			{
				if (CurrentSpawnCount < MaximumSpawnCount)
				{
					Spawn();
					Spawn2();
					Spawn3();
					timer = 0.0f;
				}
				else
				{
					spawn = false;
					Debug.Log("SPAWN LIMIT MAX");
				}
			}
		}
	}


	void Spawn()
	{

		SpawnArea = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));

	//	int spawnPointIndex = Random.Range(0, spawnPoints.Length);
		Instantiate(spawnList,spawnPoints[0].position,Quaternion.identity);
		CurrentSpawnCount++;


	}

	void Spawn2()
	{

		SpawnArea = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));

	//	int spawnPointIndex1 = Random.Range(0, spawnPoints.Length);
		Instantiate(spawnList2,spawnPoints[1].position,Quaternion.identity);
		CurrentSpawnCount1++;


	}

	void Spawn3()
	{

		SpawnArea = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));

		//int spawnPointIndex2 = Random.Range(0, spawnPoints.Length);
		Instantiate(spawnList3,spawnPoints[2].position,Quaternion.identity);
		CurrentSpawnCount2++;


	}

}


