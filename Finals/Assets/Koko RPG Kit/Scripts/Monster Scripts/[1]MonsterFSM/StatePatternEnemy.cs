﻿using UnityEngine;
using System.Collections;

public class StatePatternEnemy: MonoBehaviour
{
    [HideInInspector]
    public Animator anim;

    public float PatrolRange;   
    public float IdleTime;
	public float ChaseRange;
	public float AttackRange;
	public GameObject target;

    public Vector3 TargetPosition;



    [HideInInspector] public IEnemyState currentState;
	[HideInInspector] public ChaseState chaseState;
	[HideInInspector] public PatrolState patrolState;
	[HideInInspector] public IdleState idleState;
	[HideInInspector] public AttackState attackState;
	[HideInInspector] public DeadState deadState;
	[HideInInspector] public UnityEngine.AI.NavMeshAgent navMeshAgent;

	private void Awake()
	{

		chaseState = new ChaseState (this);
		patrolState = new PatrolState (this);
		idleState = new IdleState (this);
		attackState = new AttackState (this);
		deadState = new DeadState (this);
		navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
	}

	void Start () 
	{
		anim = GetComponent<Animator>();
		currentState = patrolState;
	
	}

	void Update () 
	{
		currentState.UpdateState ();
	}

	private void OnTriggerEnter(Collider other)
	{
		currentState.OnTriggerEnter (other);
	}
}