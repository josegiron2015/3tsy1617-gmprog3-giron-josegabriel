﻿using UnityEngine;


public class ChaseState : IEnemyState 

{
    [HideInInspector]
    public Animator anim;
	private readonly StatePatternEnemy Enemy;

	public ChaseState (StatePatternEnemy statePatternEnemy)
	{
		Enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
        
        CheckIfCanAttack ();
        Chase();
        
    }

	public void OnTriggerEnter (Collider other)
	{

	}

	public void ToPatrolState()
	{
        Enemy.currentState = Enemy.patrolState;
	}

	public void ToChaseState()
	{
        Enemy.currentState = Enemy.chaseState;
	}

	public void ToIdleState()
	{
		Enemy.currentState = Enemy.idleState;
	}

	public void ToAttackState()
	{     
        Enemy.currentState = Enemy.attackState;
	}
		
	public void ToDeadState()
	{
		Enemy.currentState = Enemy.deadState;
	}


	private void Chase()
	{
       
        if (Enemy.target != null) //pagmay laman
        {
            Enemy.navMeshAgent.Resume();
            Enemy.navMeshAgent.SetDestination(Enemy.target.transform.position);
            Enemy.TargetPosition = Enemy.transform.position;
            Enemy.anim.SetFloat("Blend", 0.666f);          
        }
        
	}

    private void CheckIfCanAttack()
	{
		if(Vector3.Distance (Enemy.transform.position, Enemy.target.transform.position) <= Enemy.AttackRange)
		{
            
            ToAttackState ();
            Enemy.navMeshAgent.Stop();
        }

        if (Vector3.Distance(Enemy.transform.position, Enemy.target.transform.position) >= Enemy.ChaseRange)
        {
            Enemy.target = null;
            ToPatrolState();
        }
    }

    void Rotate()
    {
        if (Enemy.target.transform.position != Enemy.transform.position)
        {
            Quaternion rot = Quaternion.LookRotation(Enemy.target.transform.position - Enemy.transform.position);
            float rotationX = rot.eulerAngles.y;
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            Enemy.transform.localRotation = Quaternion.Slerp(Enemy.transform.localRotation, xQuaternion, Time.deltaTime * 1f); // 120
        }
    }

   
}