﻿using UnityEngine;


public class DeadState : IEnemyState
{
	private readonly StatePatternEnemy Enemy;
	float Timer = 2.0f;

	public DeadState (StatePatternEnemy statePatternEnemy)
	{
		Enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
		
		Dead ();

	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void ToPatrolState()
	{
		Enemy.currentState = Enemy.patrolState;
		Timer = 1.0f;
	}

	public void ToIdleState()
	{
		Debug.Log ("Can't transition to same state");
	}

	public void ToChaseState()
	{
		Enemy.currentState = Enemy.chaseState;

	}

	public void ToDeadState()
	{
		Enemy.currentState = Enemy.deadState;
	}


	void Dead()
	{

		Enemy.anim.SetTrigger ("GB_Dead");	
		Enemy.navMeshAgent.Stop();
		return;
	}

	private void DetectPlayer()
	{
		
	}


}
