﻿using UnityEngine;


public class IdleState : IEnemyState
{
	private readonly StatePatternEnemy Enemy;
	float Timer = 2.0f;

	public IdleState (StatePatternEnemy statePatternEnemy)
	{
		Enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
        Timer += Time.deltaTime;
        DetectPlayer ();	
		Idle ();

	}

	public void OnTriggerEnter (Collider other)
	{
       
    }

	public void ToPatrolState()
	{
		Enemy.currentState = Enemy.patrolState;
		Timer = 1.0f;
	}

	public void ToIdleState()
	{
		Debug.Log ("Can't transition to same state");
	}

	public void ToChaseState()
	{
		Enemy.currentState = Enemy.chaseState;

	}

	public void ToDeadState()
	{
		Enemy.currentState = Enemy.deadState;
	}


	void Idle()
	{
    
        Enemy.anim.SetFloat ("Blend", 0.0f);	
        Enemy.navMeshAgent.Stop();
        if(Timer > Enemy.IdleTime)
		{
			ToPatrolState();
		}
	}
    
	private void DetectPlayer()
	{
		Collider[] col = Physics.OverlapSphere (Enemy.transform.position, Enemy.ChaseRange);
		foreach (Collider collider in col) {
			if (collider.gameObject.CompareTag ("Player")) 
			{
				Enemy.target = collider.gameObject;				
				ToChaseState ();
             
			}
		}
	}
    

}
