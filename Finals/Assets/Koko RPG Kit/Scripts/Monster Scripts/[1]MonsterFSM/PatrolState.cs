﻿using UnityEngine;
using System.Collections.Generic;
public class PatrolState : IEnemyState 

{


	private readonly StatePatternEnemy Enemy;
	
	//private float Timer = 0.0f;



	public PatrolState (StatePatternEnemy statePatternEnemy)
	{
		Enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
		DetectPlayer ();
		Patrol ();
	}


	public void OnTriggerEnter (Collider other)
	{
		
	}

	public void ToPatrolState()
	{
		Debug.Log ("Can't transition to same state");
	}


	public void ToChaseState()
	{
		Enemy.currentState = Enemy.chaseState;
	}

	public void ToIdleState()
	{
		Enemy.currentState = Enemy.idleState;
	}

	public void ToDeadState()
	{
		Enemy.currentState = Enemy.deadState;
	}


	void Patrol ()
	{
       
        Enemy.anim.SetFloat ("Blend", 0.333f);
        Enemy.navMeshAgent.Resume();

        if (Enemy.navMeshAgent.remainingDistance <= Enemy.navMeshAgent.stoppingDistance && !Enemy.navMeshAgent.pathPending)
        {
            //Debug.Log("in patrol state!!");
            
            Vector3 randomDirection = new Vector3(Random.Range(-Enemy.PatrolRange, Enemy.PatrolRange), 0.0f, Random.Range(-Enemy.PatrolRange, Enemy.PatrolRange));
            Vector3 newDestination = Enemy.transform.position + randomDirection;
            Enemy.navMeshAgent.SetDestination(newDestination);
		
            ToIdleState();

        }
        
	}

	private void DetectPlayer()
	{
		Collider[] col = Physics.OverlapSphere (Enemy.transform.position, 4f);
		foreach (Collider collider in col) {
			if (collider.gameObject.CompareTag ("Player"))
            {
				Enemy.target = collider.gameObject;
               // Enemy.target.transform.position = collider.gameObject.transform.position;
                
                ToChaseState ();
			}
		}
	}

  
}