﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
	public GameObject target;
	public float damping = 5f;
	Vector3 offset;

	void Awake()
	{
		//DontDestroyOnLoad(transform.gameObject);
	}
	void Start()
	{
		target = GameObject.Find("Koko");
		//this.transform.position = target.transform.position;
		offset = transform.position - target.transform.position;

	}

	void LateUpdate()
	{

		Vector3 desiredPosition = target.transform.position + offset;
		Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
		transform.position = position;
	}
}