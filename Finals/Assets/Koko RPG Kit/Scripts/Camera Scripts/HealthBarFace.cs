﻿using UnityEngine;
using System.Collections;

public class HealthBarFace : MonoBehaviour {

    // Camera camera;
    private Camera camera;

    void Awake()
    {
        camera = Camera.main;
    }
	void Update () {
        transform.LookAt(camera.transform);
        //So Health Bar will always look towards the camera.
       //transform.rotation = Quaternion.LookRotation(camera.transform.forward);
    }
}
