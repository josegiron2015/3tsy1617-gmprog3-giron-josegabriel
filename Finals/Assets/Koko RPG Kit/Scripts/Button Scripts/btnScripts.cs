﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class btnScripts : MonoBehaviour
{
    public

    AudioClip sound;

    private

    Button button { get { return GetComponent<Button>(); } }
    AudioSource source { get { return GetComponent<AudioSource>(); } }

    // Use this for initialization
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        source.playOnAwake = false;

    }

    public void HoverSound()
    {
        source.PlayOneShot(sound);
    }


}
