﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class StartGame : MonoBehaviour {

    public void Awake()
    {
        Time.timeScale = 1;
    }
    
    public void GameStart()
    {
        SceneManager.LoadScene("3.Field"); // Will change in the future
    }
    public void ToMainMenu()
    {
        SceneManager.LoadScene("MainMenu"); // Will change in the future
    }

    public void CheckAnimations()
    {
        SceneManager.LoadScene("Animations Scene");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
