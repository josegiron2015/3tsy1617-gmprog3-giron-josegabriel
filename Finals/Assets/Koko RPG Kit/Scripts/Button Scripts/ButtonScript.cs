﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ButtonScript : MonoBehaviour
{
    public GameObject image;
    public GameObject skillsbox;
    public GameObject healButton;
    public GameObject buffButton;
    public GameObject inventorybox;
	public GameObject questbox;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    public void ActivateAndDeactivate()
    {
        image.SetActive(!image.activeSelf);
    }

    public void ActivateAndDeactivateSkillsBox()
    {
        skillsbox.SetActive(!skillsbox.activeSelf);
    }

    public void ActivateAndDeactivateInventoryBox()
    {
        inventorybox.SetActive(!inventorybox.activeSelf);
    }

	public void ActivateAndDeactivateQuestBox()
	{
		questbox.SetActive(!questbox.activeSelf);
	}

    public void ActivateHealButton()
    {
        healButton.SetActive(true);
    }

    public void ActivateBuffButton()
    {
        buffButton.SetActive(true);
    }

    public void DeactivateBuffButton()
    {
        buffButton.SetActive(false);
    }

    public void DeactivateHealButton()
    {
        healButton.SetActive(false);
    }


}
