﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
public class KokoMoveState :  IKokoState
{
	private readonly KokoStatePattern Player;

	public KokoMoveState (KokoStatePattern statePatternEnemy)
	{
		Player = statePatternEnemy;
	}

	public void UpdateState()
	{
        RemoveOtherAnimations();
        RaycastToTarget();
        Rotate();
        Move();       
	}
		
	public void OnTriggerEnter (Collider other)
	{
       
    }
		
    public void ToKokoMoveState()
	{
		Debug.Log ("Can't transition to same state");
	}
		
	public void ToKokoChaseState()
	{
		Player.kokocurrentState = Player.kokochaseState;
	}

	public void ToKokoIdleState()
	{
		Player.kokocurrentState = Player.kokoidleState;
	}

    public void ToKokoAttackState()
    {
        Player.kokocurrentState = Player.kokoattackState;
    }

    public void RaycastToTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // another conversion chu
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000.0f))
        { 
            if (EventSystem.current.IsPointerOverGameObject())

            {
                //Do not do anything if click on canvas
            }

            else {

                if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.MonsterLayer)) // if monster was hit
                {
                    //Debug.Log("MONSTER HIT ^2");              
                    Player.Target = hit.collider.gameObject; // Hit Monster make monster as target position
                    //Player.Pointer = Instantiate(Player.Target, hit.point, Quaternion.identity) as GameObject;
                    Player.anim.SetTrigger("Run");                                        
                    ToKokoChaseState();
                }

                else if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.GroundLayer)) // if terrain was hit
                {             
                    Player.TargetPosition = hit.point; // Hit Terrain make terrain as target position
                    Player.anim.SetTrigger("RunNo");
                }
            }
        }
        
    }

    void Move()
    {
        Vector3 PlayerMovementPosition = Player.TargetPosition - Player.transform.position;
        PlayerMovementPosition.Normalize();
       
            if (Vector3.Distance(Player.transform.position, Player.TargetPosition) > Player.StopRange)
            {
                Player.charactercontroller.Move(PlayerMovementPosition * Player.speed * Time.deltaTime);
            }

            else if (Vector3.Distance(Player.transform.position, Player.TargetPosition) <= Player.StopRange)
            {
                Player.anim.SetTrigger("Idle");
                Debug.Log("Idle State");
                ToKokoIdleState();
            }
    }

    void Rotate()
    {
        if (Player.TargetPosition != Player.transform.position)
        {
            Quaternion rot = Quaternion.LookRotation(Player.TargetPosition - Player.transform.position);
            float rotationX = rot.eulerAngles.y;
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            Player.transform.localRotation = Quaternion.Slerp(Player.transform.localRotation, xQuaternion, Time.deltaTime * 9f); // 120
        }
    }

    void RemoveOtherAnimations()
    {
        Player.anim.ResetTrigger("Attack");
        //Player.anim.ResetTrigger("RunNo"); // Fix the bug in which when you click near the player, it will play the running animation even if IdleState
        
    }
}