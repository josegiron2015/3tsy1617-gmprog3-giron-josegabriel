﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class KokoAttackState : IKokoState {

  

	private readonly KokoStatePattern Player;
	public KokoStatePattern target;

	public KokoAttackState (KokoStatePattern statePatternEnemy)
	{
        
        Player = statePatternEnemy;
	}

	public void UpdateState()
	{
        RaycastToTarget();
		Attack ();
	}

	public void OnTriggerEnter (Collider other)
	{
       
	}

	public void ToKokoMoveState()
	{
        Player.kokocurrentState = Player.kokomoveState;
    }

	public void ToKokoChaseState()
	{
		Player.kokocurrentState = Player.kokochaseState;
	}

	public void ToKokoIdleState()
	{
		//Enemy.currentState = Enemy.idleState;
	}

    public void ToKokoAttackState()
    {
        
    }

    public void RaycastToTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); // another conversion chu
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000.0f))
        { 
            if (EventSystem.current.IsPointerOverGameObject())

            {
                //Do not do anything if click on canvas
            }

            else {
               // Player.anim.SetBool("Attack", false);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.MonsterLayer)) // if monster was hit
                {
                    
                    Player.Target = hit.collider.gameObject; // Hit Terrain make terrain as target position
                    Player.anim.SetTrigger("Run");
                    ToKokoChaseState();
                    

                }

                else if (Physics.Raycast(ray, out hit, Mathf.Infinity, Player.GroundLayer)) // if terrain was hit
                {
                    
                    //Debug.Log("TERRAIN HIT MOVE STATE");
                    // Player.anim.SetBool("Attack", false);
                    Player.TargetPosition = hit.point; // Hit Terrain make terrain as target position
                    Player.anim.SetTrigger("RunNo");
                    ToKokoMoveState();

                    Player.Target = null;
                    
                }
            }
        }
    }

    void Attack()
    {
        Player.anim.SetTrigger("Attack");
    }


}
