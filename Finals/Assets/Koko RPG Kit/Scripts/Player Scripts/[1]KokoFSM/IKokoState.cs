﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
public interface IKokoState
{

	void UpdateState();

	void OnTriggerEnter (Collider other);

	void ToKokoMoveState();

	void ToKokoIdleState();

	void ToKokoChaseState();

    void ToKokoAttackState();

    void RaycastToTarget();
   
  
}