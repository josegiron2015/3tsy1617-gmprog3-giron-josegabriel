﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BaseQuest : MonoBehaviour 
{
	public Text QuestNameTextBox;
	public Text QuestInfoTextBox;
	public Text TXTQuestProgress;
	//public Text CurrentProgressTextBox;
	//public Text RequiredProgressTextBox;

	public string QuestName;
	public string QuestInfo;
	public string STRQuestProgress;
	public float CurrentProgress;
	public float RequiredProgress;

	public float GoldReward;
	public float ExpReward;

	public virtual void QuestUpdate()
	{
		QuestName = "";
		QuestInfo = string.Format( "");
		STRQuestProgress = "";

		QuestNameTextBox.text = QuestName;
		QuestInfoTextBox.text = QuestInfo;
		TXTQuestProgress.text = STRQuestProgress;
	}


}
