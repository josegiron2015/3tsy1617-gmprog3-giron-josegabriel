﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class SlayQuest1 : BaseQuest 
{
	public GameObject SpawnQuest;
	public Enemy enemy;
	public Player player;
	//public List<Enemy> enemies = new List<Enemy> ();
	//Goal Number
	public float NumWildPigs;
	public float NumSlimes;
	public float NumGoblins;

	// Current Number
	public float currentWildPigs;
	public float currentSlimes;
	public float currentGoblins;


	void Start () 
	{
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player> ();
		//QuestUpdate ();
		//enemy.BigGoblinKilled.AddListener (OnBigGoblinKilled); // Function below
		//enemy.BigSlaKilled.AddListener (OnBigSlaKilled); // Function below
		//enemy.BigWildPigKilled.AddListener (OnBigWildPigKilled); // Function below

	//	AreaScout (this.transform.position, 5f, 999999);


	}
	

	public override void QuestUpdate()
	{
		QuestName = "Slayer Quest 1: The Beginning";
		QuestInfo = string.Format( " Slay " + "{0}" + " Big WildPigs" + "\n" +
								   " Slay" + " {1} " + "Big Slimes" + "\n" +
								   " Slay" + " {2} " + "Big Goblins" + "\n" , 
									NumWildPigs,NumSlimes,NumGoblins);
		
		STRQuestProgress = string.Format("{0} / {1} " + " Big WildPigs " + "\n" +
										 "{2} / {3} " + " Big Slimes " + "\n" +
										 "{4} / {5} " + " Big Goblins " + "\n" , 
											currentWildPigs,NumWildPigs,
											currentSlimes, NumSlimes,
											currentGoblins, NumGoblins);

		QuestNameTextBox.text = QuestName;
		QuestInfoTextBox.text = QuestInfo;
		TXTQuestProgress.text = STRQuestProgress;

		if (currentGoblins >= NumGoblins && currentSlimes >= NumSlimes && currentWildPigs >= NumWildPigs) 
		{
			QuestNameTextBox.text = "QUEST DONE";
			QuestInfoTextBox.text = "QUEST ACCOMPLISHED";
			TXTQuestProgress.text = "QUEST FINISHED";
			player.expPoints += this.ExpReward;
			player.GoldCount += this.GoldReward;
			Destroy (this.gameObject);
		}
			
			
	}

	void Update () 
	{
		
		QuestUpdate ();


	//	QuestUpdate ();

	}

	public void Activate()
	{
		this.gameObject.SetActive (true);
	}

	public void ActivateSpawner()
	{
		SpawnQuest.SetActive (true);
	}


	public void OnBigGoblinKilled(Enemy enemy)
	{
		currentGoblins++;

	}

	public void OnBigSlaKilled(Enemy enemy)
	{
		currentSlimes++;

	}

	public void OnBigWildPigKilled(Enemy enemy)
	{
		currentWildPigs++;

	}
		
	void AreaScoutHandler()
	{
		//if(enemies.Count <= 5)
		AreaScout (this.transform.position, 5f, 500);
	}

	void AreaScout(Vector3 location, float radius, float damage)
	{


		Collider[] objectsInRange = Physics.OverlapSphere (location, radius);
		foreach (Collider col in objectsInRange) 
		{
			Enemy enemy = col.GetComponent<Enemy> () as Enemy;
			if (enemy != null) 
			{
			//	enemies.Add (enemy);

			}

		}
		return;
	}


}
