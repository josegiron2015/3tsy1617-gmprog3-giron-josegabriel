﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestOpenerDialogue : MonoBehaviour {

	public GameObject QuestGiverDialogueBox;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	void OnTriggerStay(Collider other)
	{

		if (Input.GetKeyDown(KeyCode.E)) 
		{
			this.gameObject.SetActive(!this.gameObject.activeSelf);
			Debug.Log ("Shop Enabled");
		}

		if (other.CompareTag ("Player")) 
		{
			QuestGiverDialogueBox.SetActive (true);

		}
	}

	void OnTriggerExit(Collider other)
	{

		if (other.CompareTag ("Player")) 
		{
			this.gameObject.SetActive (false);

		}
	}


}
