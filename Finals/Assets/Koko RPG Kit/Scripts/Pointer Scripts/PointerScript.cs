﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PointerScript : MonoBehaviour {


    public GameObject DefaultPointer; // object to Instantiate
    public GameObject EnemyPointer; // object to Instantiate
  
    public GameObject Pointer;    
    public GameObject MonsterPointer;

    public LayerMask GroundLayer;
    public LayerMask MonsterLayer;

    //addon toppings
  
    // Use this for initialization
    void OnTriggerEnter(Collider other)
    {
      if(other.gameObject.CompareTag("Player"))
        {
            Debug.Log("DETECTED");
            Destroy(this.gameObject);
        }
    }
    void Start ()
    {

       
    }
	
	// Update is called once per frame
	void Update ()
    {

        RaycastToTarget();
     

    }

    public void RaycastToTarget ()
    {

       
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); //
        RaycastHit hit;

        
        
        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 1000.0f))
        {
            
            Destroy(Pointer);
            Destroy(MonsterPointer);
            if (EventSystem.current.IsPointerOverGameObject())

            {
                //Do not do anything if click on canvas
            }

            else {

                if (Physics.Raycast(ray, out hit, 1000.0f, MonsterLayer))
                {
                    //PointerHolder = hit.collider.gameObject;
                    // MonsterLocation = hit.collider.gameObject.transform;
                   
                    MonsterPointer = Instantiate(EnemyPointer, hit.collider.gameObject.transform.position, Quaternion.identity) as GameObject;
                    MonsterPointer.transform.SetParent(hit.collider.gameObject.transform);



                }

               else if (Physics.Raycast(ray, out hit, 1000.0f, GroundLayer))
                {
                    Pointer = Instantiate(DefaultPointer, hit.point, Quaternion.identity) as GameObject;
                }

              

            }
            
        }

    }

  
    

   
}
