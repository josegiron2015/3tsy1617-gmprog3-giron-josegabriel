﻿using UnityEngine;
using System.Collections;

public class PointerDetection : MonoBehaviour {

	void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.CompareTag("Pointer"))
        {
            Destroy(other.gameObject);
        }
    }
}
