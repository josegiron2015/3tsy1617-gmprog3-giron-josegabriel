﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealSkill : Skills
{
    public Animator anim;
    public Player player;
    public Button healButton;
    public float CurrentCoolDownValue;
    public float MaximumCoolDownValue = 10f;

    public Text cooldown;

    bool TimerOff = false;
    bool CoolDownOff = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        player = GetComponent<Player>();

        CurrentCoolDownValue = MaximumCoolDownValue;
    }
    public override void UseSkill()
    {
        player.hitPoints += 10f;
        player.anim.SetTrigger("Damage");

        TimerOff = true;
        CoolDownOff = true;

    }
    void Update()
    {
        

        if (CoolDownOff)
        {
            cooldown.enabled = true;
            cooldown.text =
            CurrentCoolDownValue.ToString("f0");
            healButton.interactable = false;
            CurrentCoolDownValue -= Time.deltaTime;


            if (CurrentCoolDownValue <= 0f)
            {
                CurrentCoolDownValue = MaximumCoolDownValue;
                CoolDownOff = false;
                healButton.interactable = true;
                cooldown.enabled = false;
            }
        }
        HealthChecker();
    }


   

    public void HealthChecker()
    {
        if (player.hitPoints >= player.maxHitPoints)
        {
            player.hitPoints = player.maxHitPoints;
        }
    }

}
