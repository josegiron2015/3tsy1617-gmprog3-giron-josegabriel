﻿using UnityEngine;
using System.Collections;

public class LevelUpMechanics : MonoBehaviour
{
    public Player player;

    public float CurrentStatPoints;
    public float MaxStatPoints;

    
    public GameObject LevelUpObjects; //PangLevelUp ng str or vit
    void Start()
    {
        player = GetComponent<Player>();
        CurrentStatPoints = 0f;
        
    }
    void Update()
    {
        if(CurrentStatPoints >= MaxStatPoints)
        {
            LevelUpObjects.SetActive(false);
            CurrentStatPoints = 0f;
            MaxStatPoints = 0f;
        }
    }
   public void StrengthAddPoints()
    {
        CurrentStatPoints += 1f;
        player.str += 1f;
    }

   public void VitalityAddPoints()
    {
        CurrentStatPoints += 1f;
        player.vit += 1f;
    }

}
