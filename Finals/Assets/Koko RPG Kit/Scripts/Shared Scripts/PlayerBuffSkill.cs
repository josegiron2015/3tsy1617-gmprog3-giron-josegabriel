﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerBuffSkill : Skills {

    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public Player player;
    [HideInInspector]
    public Button buffButton;
    [HideInInspector]
    public KokoStatePattern kokostatepattern;
    [HideInInspector]
    public Text cooldown;

   public float CurrentCoolDownValue;
   public float MaximumCoolDownValue = 10f; // can be changed

   public float StrBuffValue = 50f; // can be changed
   public float SpeedBuffValue = 10f; // can be changed

   public float CurrentBuffTime;
   public float MaxBuffTime = 5f; // can be changed

    bool TimerOff = false; // checkers
    bool CoolDownOff = false; // checkers
    void Start ()
    {
        anim = GetComponent<Animator>();
        player = GetComponent<Player>();
        kokostatepattern = GetComponent<KokoStatePattern>();

        CurrentBuffTime = MaxBuffTime;
        CurrentCoolDownValue = MaximumCoolDownValue;
	}

    public override void UseSkill() // Buff Powers Here
    {
        player.str += StrBuffValue;
        kokostatepattern.speed += SpeedBuffValue;

        if (CurrentBuffTime >= MaxBuffTime)
        {                       
            TimerOff = true;
            CoolDownOff = true;        
        }
       
        player.anim.SetTrigger("Damage");
        
    }
    void Update ()
    {

        if (TimerOff)
        {
            
            CurrentBuffTime -= Time.deltaTime;
            

            buffButton.interactable = false;

            if (CurrentBuffTime <= 0f) // Buff Anti - Powers Here
            {
                
                player.str -= StrBuffValue;
                kokostatepattern.speed -= SpeedBuffValue;
                CurrentBuffTime = MaxBuffTime;
                TimerOff = false;
                //buffButton.interactable = true;
            }
 
        }

        if (CoolDownOff)
        {
            cooldown.enabled = true;
            cooldown.text =
            CurrentCoolDownValue.ToString("f0");

            CurrentCoolDownValue -= Time.deltaTime;

            if (CurrentCoolDownValue <= 0f)
            {
                CurrentCoolDownValue = MaximumCoolDownValue;
                CoolDownOff = false;
                buffButton.interactable = true;
                cooldown.enabled = false;
            }
        }

    }

}
