﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class Player : Stats {

    //Player Stuff 

    //Images
    [HideInInspector]
    public Image kokoHealthBar;
    [HideInInspector]
    public Image KokoExpBar;

    //Texts
    [HideInInspector]
    public Text statBox;
    [HideInInspector]
    public Text goldBox;
    [HideInInspector]
    public Text healText;
    [HideInInspector]
    public Text buffText;
    [HideInInspector]
    public Text levelText;
    [HideInInspector]
    public Text statPoints;

	public Text TXT_ManaPoints;

    //Floats
    public float vit;
    public float str;
    [HideInInspector]
    public float expPoints;
    public float maxEXPPoints;
    public float GoldCount;
	public float manaPoints;


	//Bookmarked <3
	public Inventory inventory;

    //Buttons
    [HideInInspector]
    public Button healButton;
    [HideInInspector]
    public Button buffButton;

    //GameObjects
    [HideInInspector]
    public GameObject levelUpObjects;
    [HideInInspector]
    public GameObject target; // important
    [HideInInspector]
    public GameObject statButtons;
   

	public List<GameObject> dropDatabase = new List<GameObject>();
    //Scripts
    [HideInInspector]
    public Enemy enemy; // get my enemy script bwahaha
    [HideInInspector]
    public Animator anim; // my koko's animation script
    [HideInInspector]
    public KokoStatePattern kokostatepattern;
    [HideInInspector]
    public LevelUpMechanics levelupmechanics;

    private float StatPointsRemaining;


    // Use this for initialization
    void Awake()
    {
       
    }
    void Start ()
    {

        kokostatepattern = GetComponent<KokoStatePattern>();
        anim = GetComponent<Animator>();
        levelupmechanics = GetComponent<LevelUpMechanics>();
        LevelUp(); // why is this in start? para hindi level 0 sa umpisa
        //Formula


        hitPoints = maxHitPoints;
       // EXPPoints = maxEXPPoints;

       kokoHealthBar.fillAmount = hitPoints / maxHitPoints;
       KokoExpBar.fillAmount = expPoints / maxEXPPoints;

        GoldCount = 0f;
       
    }
	
	// Update is called once per frame
	void Update ()
    {
       
            HealthFillamountUpdater(); // Para realtime
            ActivateQuickly(); // Formula lang
            Statistics(); // Koko's Stats

        if (kokostatepattern.Target != null)
        {
            target = kokostatepattern.Target; // target will be what koko will target 
            enemy = target.GetComponent<Enemy>();

         //   IsTargetDead(); // Is Enemy target dead?
            LevelUp(); // I shall Level Up
			return;
        }
    }

    void DamageTo()
    {
        
        Debug.Log("Player using DamageTo Function");     
        enemy.hitPoints -= AttackDMG; 
        enemy.EnemyHealthBar.fillAmount = enemy.hitPoints / enemy.maxHitPoints;
        //enemy.anim.SetFloat("Blend", 3f);
    }

    void AreaDamageToMonsters()
    {
        enemy.hitPoints -= AttackDMG;
    }

    public void IsTargetDead()
    {
		Vector3 dropPosition = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
        if (enemy.hitPoints <= 0) // if enemy's health is less than 0
        {
            // What will happen to enemy
            Debug.Log("Target Neutraulized");

            expPoints += enemy.expHold; // give experience to koko

           // enemy.statepatternenemy.anim.SetTrigger("GB_Dead"); // enemy will play dead animation
            
            kokostatepattern.kokocurrentState = kokostatepattern.kokoidleState; // koko will go to idle state
            kokostatepattern.Target = null; // enemy will not have any target

            kokostatepattern.anim.SetTrigger("Put"); // koko will do Put sword animation 

            KokoExpBar.fillAmount = expPoints / maxEXPPoints; // Koko's Exp Bar

			//Instantiate(gold, enemy.transform.position + dropPosition, Quaternion.identity);

            //Destroy Target's scripts
            Destroy(target.GetComponent<BoxCollider>());
            Destroy(target.GetComponent<StatePatternEnemy>());
            Destroy(target.GetComponent<Enemy>());
            Destroy(target, 3f);
        }

    }

    void ActivateQuickly()
    {
        maxHitPoints = vit * 2;
        AttackDMG = str * 2;

        goldBox.text =
            GoldCount.ToString();

		TXT_ManaPoints.text = manaPoints.ToString ();
    }
    void LevelUp()
    {
       
        if (expPoints >= maxEXPPoints - 1f)
        {
            expPoints = 0.0f;
            maxEXPPoints += 20f;    
            maxHitPoints += 10f;          
            // if nag level up, strength and vitality + 2
            //str += 2f;
            //vit += 2f;

            Level += 1f;
            levelupmechanics.MaxStatPoints += 5f;
            //Formula
            maxHitPoints = vit * 2;
            AttackDMG = str * 2;

            hitPoints = maxHitPoints;
            KokoExpBar.fillAmount = expPoints;            
            kokoHealthBar.fillAmount = maxHitPoints;

            statButtons.SetActive(true); //
            
            //// Skills
            if(Level >= 2f)
            {
                healButton.interactable = true;
                healText.text = "Available";
            }

            if (Level >= 3f)
            {
                buffButton.interactable = true;
                buffText.text = "Available";
            }


        }
    }
    
    void Statistics()
    {
            statBox.text =
            "Name : " + name +
            "\n\nLevel : " + Level +
            "\n\nHP : " + hitPoints + "/" + maxHitPoints +
			"\n\nMANA : " + manaPoints +
            "\n\nAttack : " + AttackDMG +
            "\n\nVit : " + vit +
            "\n\nSTR : " + str +
            "\n\nGold : " + GoldCount +
            "\n\nExp : " + expPoints + "/" + maxEXPPoints;

            goldBox.text = 
            GoldCount.ToString();

            levelText.text =
            Level.ToString();

		TXT_ManaPoints.text = manaPoints.ToString ();

        StatPointsRemaining = levelupmechanics.MaxStatPoints - levelupmechanics.CurrentStatPoints;

        statPoints.text =
            StatPointsRemaining.ToString();

        
        
    }

   void HealthFillamountUpdater()
    {
        kokoHealthBar.fillAmount = hitPoints / maxHitPoints;
    }

   void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Gold"))
        {
            GoldCount += enemy.goldHold;
            Destroy(other.gameObject);
        }



		if (other.tag == "Item") //If we collide with an item that we can pick up
		{
			inventory.AddItem(other.GetComponent<Item>()); //Adds the item to the inventory.
			Debug.Log("Inventory Added");
		}
    }



    void StartEffect()
    {
        //pampatanggal ng error
    }
    void StopEffect()
    {
        //pampatanggal ng error
    }
}
