﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
public class MonsterKilled : UnityEvent<Enemy>{}

public class Enemy : Stats 
{

	//Events
	public MonsterKilled GoblinKilled = new MonsterKilled ();
	public MonsterKilled SlaKilled = new MonsterKilled ();
	public MonsterKilled WildPigKilled = new MonsterKilled ();

	public SlayQuest1 Quest;
	//public GameObject QuestGO;
	//Quest Events
	public MonsterKilled BigGoblinKilled = new MonsterKilled ();
	public MonsterKilled BigSlaKilled = new MonsterKilled ();
	public MonsterKilled BigWildPigKilled = new MonsterKilled ();
    //Player related
	//[HideInInspector]
	public GameObject gold; 
    [HideInInspector]
    public Player player;
    [HideInInspector]
	public StatePatternEnemy statepatternenemy;
    [HideInInspector]
    public Animator anim;
    [HideInInspector]
    public Image EnemyHealthBar;

    public float vit;
    public float str;
    public float expHold;
    public float goldHold;
	public float rotSpeed;
	bool canDropGold = true;
    public GameObject target; // gets the target (koko)
	public GameObject UnitCanvas;

   

    // Use this for initialization
    void Start ()
    {
		Debug.Log("TESTES");
		Quest = (SlayQuest1)FindObjectOfType (typeof(SlayQuest1));

		AttackDMG = str * 2; // Damage
		maxHitPoints = vit * 2; // max health
		hitPoints = maxHitPoints; // my health = maximum health
		EnemyHealthBar.fillAmount = hitPoints / maxHitPoints; // 

		statepatternenemy = GetComponent<StatePatternEnemy>();   
		//target = statepatternenemy.target; // Target of enemy

		LeanTween.init (1000);
		rotSpeed = 0.5f;          
        


      
    }

	void ChaseTarget() // Function to check and know that Koko is the target, nagiging null kasi ang target kapag nilagay sa update
	{    
		target = statepatternenemy.target; // Target of enemy
		Debug.Log (this.name + target);
		// player.anim.SetTrigger("Damage"); // Makes Koko Twitch AHAHAHAH
	}

	// Update is called once per frame
	void Update ()
    {
		

		EnemyHealthBar.fillAmount = hitPoints / maxHitPoints; // is in update because of personal stuff

		if (target == null)
        {
			
            return;
        }


		AmIDead ();
//        else if (target != null)
//        {
//			
//            IsTargetDead();
//        }
    }


     void DamageTo()
    {    
		
		player = target.GetComponent<Player>(); 
		Debug.Log(this.name + " using DamageTo Function");
        player.hitPoints -= AttackDMG; 
        player.kokoHealthBar.fillAmount = player.hitPoints / player.maxHitPoints; 
       // player.anim.SetTrigger("Damage"); // Makes Koko Twitch AHAHAHAH
    }

	public void AmIDead()
	{
		

		float deathSpeed = rotSpeed * Time.deltaTime;
		Debug.Log (this.hitPoints);
		if (this.hitPoints <= 0f) // if Koko's health is less than 0
		{
			if (canDropGold) 
			{
				DropGold ();
				canDropGold = false;





				//QuestGO = GameObject.FindGameObjectWithTag ("Quest");
				//Quest = QuestGO.GetComponent<SlayQuest1> ();



				switch (this.GetComponent<EnemyName> ().Name)
				{

				case "Goblin":
					GoblinKilled.Invoke (this);
					Debug.Log (" A Goblin Was Killed");
					break;

				case "Sla":
					SlaKilled.Invoke (this);
					Debug.Log(" A Sla Was Killed");
					break;

				case "WildPig":
					WildPigKilled.Invoke (this);
					Debug.Log(" A WildPig Was Killed");
					break;

					// Quest Monsters temporary

				case "BIGGoblin":
					BigGoblinKilled.Invoke (this);
					Debug.Log (" A Quest Goblin Was Killed");
					Quest.currentGoblins++;
					break;

				case "BIGSla":
					BigSlaKilled.Invoke (this);
					Debug.Log(" A Quest Sla Was Killed");
					Quest.currentSlimes++;
					break;

				case "BIGWildPig":
					BigWildPigKilled.Invoke (this);
					Debug.Log(" A Quest WildPig Was Killed");
					Quest.currentWildPigs++;
					break;

				default:
					break;
				}

			}


			//Killed.Invoke (this); // Monster is Dead
			statepatternenemy.currentState = statepatternenemy.deadState; // enemy will go to dead state
			//statepatternenemy.target = null; // enemy will not have any target
			LeanTween.moveY( this.gameObject, -100f, 3f).setEase(LeanTweenType.easeInQuad ).setDelay(2f);
			//Destroy(target.GetComponent<CharacterController>());
			//Destroy(target.GetComponent<KokoStatePattern>());
			//Destroy(target.GetComponent<PointerScript>());
			//Destroy(target.GetComponent<PointerDetection>());
			Destroy(this.GetComponent<BoxCollider>());
			Destroy(UnitCanvas);
			Destroy(this.gameObject,7f);
			//LeanTween.alpha (this.gameObject, 0, 3);
			// What will happen to Koko
			//this.anim.SetTrigger("GB_Dead"); // she will play dead animation
			//this.hitPoints = 0; // para walang negative
			//this.maxHitPoints = 0; // para walang level Up

			return;
		}

	}

	public void DropGold()
	{
		Vector3 dropPosition = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
		Instantiate(gold, this.transform.position + dropPosition, Quaternion.identity);
	}

//    public void IsTargetDead()
//    {
//        if (player.hitPoints <= 0f) // if Koko's health is less than 0
//        {
//            // What will happen to Koko
//            player.anim.SetTrigger("Dead"); // she will play dead animation
//            player.hitPoints = 0; // para walang negative
//            player.maxHitPoints = 0; // para walang level Up
//            statepatternenemy.currentState = statepatternenemy.idleState; // enemy will go to patrol state
//            statepatternenemy.target = null; // enemy will not have any target
//            
//            Destroy(target.GetComponent<CharacterController>());
//            Destroy(target.GetComponent<KokoStatePattern>());
//            Destroy(target.GetComponent<PointerScript>());
//            Destroy(target.GetComponent<PointerDetection>());
//            Destroy(target.gameObject,5f);
//
//                               
//        }

 //   }

  
}
