﻿using UnityEngine;
using System.Collections;

public class PlayerCombo : Skills
{

    public Enemy enemy;
    public Player player;
    public Animator anim;
    public KokoStatePattern kokostatepattern;
    public GameObject target;
    // Use this for initialization
    void Start()
    {
        enemy = GetComponent<Enemy>();
        player = GetComponent<Player>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
		//enemy.EnemyHealthBar.fillAmount = enemy.hitPoints / enemy.maxHitPoints;
    }

    public override void UseSkill()
    {
        player.anim.SetTrigger("Skill");
		this.GetComponent<Player> ().manaPoints -= 30;

    }

	public  void AreaAttack()
    {      
	 AreaDamageEnemies(this.transform.position, 5f, 500);
		/*
		Collider[] objectsInRange = Physics.OverlapSphere (location, radius);
		foreach (Collider col in objectsInRange) 
		{

			Enemy enemy = col.GetComponent<Enemy> ();
			if (enemy != null) 
			{
				Debug.Log ("Called");
				float proximity = (location - enemy.transform.position).magnitude;
				float effect = 1 - (proximity / radius);
				enemy.hitPoints -= damage;

			}
		}*/

    }



	void AreaDamageEnemies(Vector3 location, float radius, float damage)
	{
		
		Collider[] objectsInRange = Physics.OverlapSphere (location, radius);
		foreach (Collider col in objectsInRange) 
		{
			
			Enemy enemy = col.GetComponent<Enemy> () as Enemy;
			if (enemy != null) 
			{
				Debug.Log ("Area Damage" + enemy.gameObject.name + " : " + enemy.hitPoints);
				//float proximity = (location - enemy.transform.position).magnitude;
				//float effect = 1 - (proximity / radius);
				enemy.hitPoints -= damage;

			}
		}
	}


  }

