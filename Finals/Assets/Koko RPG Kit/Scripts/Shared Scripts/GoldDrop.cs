﻿using UnityEngine;
using System.Collections;

public class GoldDrop : MonoBehaviour
{
    public Player player;
    public Enemy enemy;
    public float MinGoldCount = 5f;
    public float MaxGoldCount = 10f;
	// Use this for initialization
	void Start ()
    {       
        player = GetComponent<Player>();
        enemy = GetComponent<Enemy>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider other)
    {     
        
        //Destroy(other.gameObject);
         if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Gold Detected");
            player.GoldCount += enemy.goldHold;   //medj madaya        
        }

       else if (other.gameObject == null)
        {
            return;
        }
    }
}
