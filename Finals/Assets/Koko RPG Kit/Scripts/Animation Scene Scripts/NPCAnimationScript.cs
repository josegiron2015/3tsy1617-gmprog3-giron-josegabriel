﻿using UnityEngine;
using System.Collections;

public class NPCAnimationScript : MonoBehaviour {

    private Animator animator;

    void Start ()
    {
        animator = GetComponent<Animator>();
    }

    public void NPCsAnimation(string animation)
    {
        animator.SetTrigger(animation);
    }
}
