﻿using UnityEngine;
using System.Collections;

public class MonstersAnimationScript : MonoBehaviour {

    private Animator animator;
    
	void Start ()
    {
        animator = GetComponent<Animator>();
	}
	
	public void MonstersAnimation(string animation)
    {
        animator.SetTrigger(animation);
    }
}
