﻿using UnityEngine;
using System.Collections;

public class CameraAnimationScript : MonoBehaviour {

    public Camera cam;

    private Vector3 playercamPosition;
    private Quaternion playercamRotation;

    private Vector3 monstercamPosition;
    private Quaternion monstercamRotation;

    private Vector3 npccamPosition;
    private Quaternion npccamRotation;

    // Use this for initialization
    void Start ()
    {

        cam = GetComponent<Camera>();

        //Cameras
        playercamPosition = new Vector3(8f, 3f, 10f);
        playercamRotation = Quaternion.Euler(4.3327f, 178.5991f, 359.9971f);

        monstercamPosition = new Vector3(-13.49262f, 3f, 5.342255f);
        monstercamRotation = Quaternion.Euler(8.286f, 179.5623f, 359.9971f);

        npccamPosition = new Vector3(-37.04944f, 1.61014f, 6.726052f);
        npccamRotation = Quaternion.Euler(8.6297f, 183.3438f, 359.9971f);

        //Starting position upon start
        cam.transform.position = playercamPosition;
        cam.transform.rotation = playercamRotation;
    }

    public void PlayerCamera()
    {
        cam.transform.position = playercamPosition;
        cam.transform.rotation = playercamRotation;
    }

    public void MonsterCamera()
    {
        cam.transform.position = monstercamPosition;
        cam.transform.rotation = monstercamRotation;
    }

    public void NpcCamera()
    {
        cam.transform.position = npccamPosition;
        cam.transform.rotation = npccamRotation;
    }


}
