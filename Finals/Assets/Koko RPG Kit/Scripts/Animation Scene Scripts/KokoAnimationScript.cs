﻿using UnityEngine;
using System.Collections;

public class KokoAnimationScript : MonoBehaviour {

    private Animator animator;
    
    void Start()
    {
        //Components
        animator = GetComponent<Animator>();
             
    }
	public void AnimationController(string animation)
    {
        animator.SetTrigger(animation); 
    }

   
}
