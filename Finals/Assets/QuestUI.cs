﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class QuestUI : MonoBehaviour {

	public string QuestName;
	public string QuestInfo;
	public string STRQuestProgress;

	public Text QuestNameTextBox;
	public Text QuestInfoTextBox;
	public Text TXTQuestProgress;
	SlayQuest1 Quest;
	// Use this for initialization
	void Start () 
	{
		Quest = (SlayQuest1)FindObjectOfType (typeof(SlayQuest1));
	}
	
	// Update is called once per frame
	void Update () 
	{
		QuestNameTextBox.text = Quest.QuestName;
		QuestInfoTextBox.text = Quest.QuestInfo;
		TXTQuestProgress.text = Quest.STRQuestProgress;
	}
}
