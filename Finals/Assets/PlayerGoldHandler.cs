﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGoldHandler : MonoBehaviour
{
	public int Value = 0;
	public Enemy enemy;
	//public Player player;

	void Start()
	{
		enemy = this.GetComponent<Enemy> ();
		//player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player> (); // I have no other idea in mind
	
		enemy.GoblinKilled.AddListener (OnGoblinKilled); // Function below
		enemy.SlaKilled.AddListener (OnSlaKilled); // Function below
		enemy.WildPigKilled.AddListener (OnWildPigKilled); // Function below
	}



	public void OnGoblinKilled(Enemy enemy)
	{
		enemy.player.GoldCount += 20;

	}

	public void OnSlaKilled(Enemy enemy)
	{
		enemy.player.GoldCount += 30;

	}

	public void OnWildPigKilled(Enemy enemy)
	{
		enemy.player.GoldCount += 50;

	}
}
