﻿using UnityEngine;
using System.Collections;

public enum ItemType {MANA, HEALTH, BUFF};

public class Item : MonoBehaviour 
{
	//Direct Reference
	public GameObject Koko;

    /// <summary>
    /// The current item type
    /// </summary>
    public ItemType type;

    /// <summary>
    /// The item's neutral sprite
    /// </summary>
    public Sprite spriteNeutral;

    /// <summary>
    /// The item's highlighted sprite
    /// </summary>
    public Sprite spriteHighlighted;

    /// <summary>
    /// The max amount of times the item can stack
    /// </summary>
    public int maxSize;

    /// <summary>
    /// Uses the item
    /// </summary>
    public void Use()
    {
        switch (type) //Checks which kind of item this is
        {
            case ItemType.MANA:
                Debug.Log("I just used a mana potion");
			Koko.gameObject.GetComponent<Player> ().manaPoints += 69;
			Debug.Log (Koko.gameObject.GetComponent<Player> ().manaPoints);
                break;

		case ItemType.HEALTH:
			Debug.Log ("I just used a health potion");
			Koko.gameObject.GetComponent<Player> ().hitPoints += 50;
			Debug.Log (Koko.gameObject.GetComponent<Player> ().hitPoints);
                break;

		case ItemType.BUFF:
			Debug.Log("I just used a Buff potion");
			Koko.gameObject.GetComponent<PlayerBuffSkill> ().UseSkill();
			break;
        }

    }

}
